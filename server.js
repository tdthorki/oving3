var express = require("express");
var app = express();
app.get("/hello", (req, res) => {
    res.send("Hello World");
});
app.get("/hello2", (req, res) => {
    res.json({
        'message': "Hello world"
    });
});

var server = app.listen(8080);